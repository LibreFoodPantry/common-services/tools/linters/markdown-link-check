# markdown-link-check

[markdown-link-check](https://github.com/tcort/markdown-link-check) is a
linter to check links in markdown files.

## Using in the Pipeline

To enable markdown-link-check in your project's pipeline add
`markdown-link-check` to the `ENABLE-JOBS`
variable in your project's `.gitlab-ci.yaml` e.g.:

```bash
variables:
  ENABLE_JOBS: "markdown-link-check"
```

The ENABLE_JOBS variable is a comma-separated string value, eg.
`ENABLE_JOBS="job1,job2,job3"`

## Using in Visual Studio Code

Install [HTTP/s and relative link checker](https://marketplace.visualstudio.com/items?itemName=blackmist.LinkCheckMD)
for Visual Studio Code.

To test for broken links with this extension, you have to hit Alt+L to
generate a log.

## Using Locally in Your Development Environment

markdown-link-check can be run locally in a linux-based bash shell or in a
linting shell script with this Docker command:

```bash
docker run --rm -v "${PWD}":/tmp:ro -w /tmp
     ghcr.io/tcort/markdown-link-check:stable
     $(find . -name '*.md' -printf '%p ')
```

## Configuration

There are no settings to configure.

## Licensing

* Code is licensed under [GPL 3.0](documentation/licenses/gpl-3.0.txt).
* Content is licensed under
  [CC-BY-SA 4.0](documentation/licenses/cc-by-sa-4.0.md).
* Developers sign-off on [DCO 1.0](documentation/licenses/dco.txt)
  when they make contributions.
